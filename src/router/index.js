import Vue from 'vue'
import Router from 'vue-router'
import Hello from '@/components/Hello'
import Chuck from '@/components/Chuck'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Hello',
      component: Hello
    },
    {
      path: '/chuck',
      name: 'Chuck',
      component: Chuck}
  ]
})
